import React, {Component} from 'react';
import './login-compilado.css';
//import jss from 'jss';
//import fondo from '../images/cinema.jpg';




export default class Login extends Component{
    constructor(props){
       super(props)
    //    this.changeHandler = this.changeHandler.bind(this)
        this.state = {
            usuario: '',
            pass:''
        }
        this.botonClick = this.botonClick.bind(this)
    }  
    changeHandler (tipo , event){
        let newState = Object.assign({}, this.state);
        newState[tipo] = event.target.value; 
        this.setState(newState)
    }
    botonClick (){
        this.props.botonClick({usuario: this.state.usuario, pass: this.state.pass})
    } 
//---------------PRUEBA DE ESTILOS REACT STYLE----------------------------------------------//




    render(){
        return (
               <div className="pagina"> 
                    <div className="cuadrado">
                        <div className="formulario">
                            <span className="cabecera"><h2>Bienvenido</h2></span>
                            <label className="etiqueta1">Usuario:</label>
                            <input placeholder="Usuario" className="input1" value={this.state.usuario} onChange={this.changeHandler.bind(this, 'usuario')}/><br/>
                            <label className="etiqueta2">Password: </label>
                            <input placeholder="contraseña" className="input2" value = {this.state.pass} onChange={this.changeHandler.bind(this, 'pass')}/>
                            <button className="botonazo" onClick={this.botonClick}>Entrar</button>    
                        </div>
                    </div>
                </div>     
           
        )
    }
}