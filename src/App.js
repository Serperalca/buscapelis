import React, {Component} from 'react';
import './App.css';
// import Login from './Loggin/login';
import Vista2 from './vistas/vista2/vista2';
//import user from '../src/images/user.png';
//import candado from '../src/images/candado.png';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import VistaLogin from './vistas/vistaLogin/vistaLogin';
import VistaDetalles from './vistas/vistaDetalles/vistaDetalles';

export default class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      estadoLogin: true,
    };  
  }
   


  render(){
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={VistaLogin} botonClick={this.botonClick}/>
          <Route path="/vista2" component={Vista2}/>
          <Route path="/detalles" component={VistaDetalles}/>
          <Redirect to="/" />
        </Switch>
      </BrowserRouter>
            
    );
  } 
} 
  

