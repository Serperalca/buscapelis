import React, {Component} from 'react';
import "./pelicula-compilado.css";

export default class Pelicula extends Component{
    constructor(props){
        super(props);
        this.state = {
            titulo : props.titulo,
            imagen : props.imagen
        }
    }



    render(){
        return(
            <div>
                <div className="titulo">{this.state.titulo}</div>
                <div className="imagen">
                    <img alt="Portada Pelicula" src={this.state.imagen}></img>
                </div>
            </div>

        )
    }

}
