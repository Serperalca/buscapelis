import React, {Component} from 'react';
import './vista2-compilado.css';
import ComponenteBusqueda from '../../Componente Busqueda/ComponenteBusqueda';
import RestService from '../../servicios/RestService';
import Pelicula from '../../Componente Pelicula/pelicula'


export default class Vista2 extends Component{
    constructor(props){
        super(props);
        this.botonClick = this.botonClick.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.props.location.state  ?
        this.state = {
            titulo : this.props.location.state.estados.titulo,
            imagen : this.props.location.state.estados.imagen,
            año    : '',
            actores: '',
            director: '',
            genero:  '',
            duracion: '',
            productora: ''
        } :
        this.state = {
            titulo : '',
            imagen : '',
            año    : '',
            actores: '',
            director: '',
            genero:  '',
            duracion: '',
            productora: '' 
        }
  
    }
    botonClick(peli){           
       let movie = encodeURIComponent(peli.pelicula);
        var url = "https://www.omdbapi.com/?t="+movie+"&apikey=2a0b18";
       RestService.llamada(url).then((respuesta)=>{
        this.setState({
            titulo:     respuesta.Title,
            imagen:     respuesta.Poster,
            año:        respuesta.Year,
            actores:    respuesta.Actors,
            director:   respuesta.Director,
            genero:     respuesta.Genre,
            duracion:   respuesta.Runtime,
            productora: respuesta.Production 
        })
    });
    console.log('se está llamando mientras seguimos con nuestro cdigo')
     
    }   

    handleClick=()=>{
        var estados = this.state;
        this.props.history.push('/detalles', {estados})
  
    }

    render(){
        return(
            <div className="pagina">
                <div>
                    <ComponenteBusqueda botonClick={this.botonClick} />
                        { 
                            this.state.titulo.length > 0 ? 
                            <div>
                                <div>
                                    <Pelicula titulo={this.state.titulo} imagen={this.state.imagen} handleClick={this.handleClick}/>
                                </div>
                                <div> 
                                    <button className="btn-detalles" onClick={this.handleClick}>Detalles</button>
                                </div>
                            </div>: 
                            ''
                        }         
                </div>
            </div>
        )
    }
}

/*
()=>{
    if(this.state.titulo.length > 0 ){
        return  <Pelicula titulo={this.state.titulo} imagen={this.state.imagen}/> 
    }  */