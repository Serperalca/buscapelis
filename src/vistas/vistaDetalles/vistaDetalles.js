import React, {Component} from 'react';
import "./detalles-compilado.css";

export default class VistaDetalles extends Component{
    constructor(props){
        super(props)
        this.botonClick = this.botonClick.bind(this)
        this.state = {
            titulo : this.props.location.state.estados.titulo,
            imagen : this.props.location.state.estados.imagen,
            año: this.props.location.state.estados.año,
            actores: this.props.location.state.estados.actores,
            director: this.props.location.state.estados.director,
            genero: this.props.location.state.estados.genero,
            duracion: this.props.location.state.estados.duracion,
            productora: this.props.location.state.estados.productora
            
        }

        
    }
    
botonClick(){
    var estados = this.state;
    this.props.history.push('/vista2', {estados});
}



    render(){
        return (
            <div className="pagina">
                <div className="cabecera">Detalles</div>
                <div className="padre">
                    <div className="DivIzquierdo">
                        <img alt="Imagen cartelera" src={this.state.imagen}></img>
                    </div>
                    <div className="DivDerecho">
                        <label className="etiqueta">Título: {this.state.titulo}</label>
                        <label className="etiqueta">Fecha de Estreno: {this.state.año}</label>
                        <label className="etiqueta">Actores: {this.state.actores}</label>
                        <label className="etiqueta">Director: {this.state.director}</label>  
                        <label className="etiqueta">Género: {this.state.genero}</label>    
                        <label className="etiqueta">Duración: {this.state.duracion}</label>
                        <label className="etiqueta">Productora:  {this.state.productora}</label>  
                    </div>
                    <button className="volver" onClick={this.botonClick}>Volver</button>
                </div>
            </div>     
        )
    }
}