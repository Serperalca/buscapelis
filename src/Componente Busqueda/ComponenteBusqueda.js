import React, {Component} from 'react';
import "./ComponenteBusqueda-compilado.css"


export default class ComponenteBusqueda extends Component{
    constructor(props){
        super(props);
        this.botonClick = this.botonClick.bind(this);
        this.state = {
            pelicula : ''
        };
        this.changeHandler = this.changeHandler.bind(this)
          
    }
    changeHandler( event){
//      changeHandler (tipo , event){
//      let newState = Object.assign({}, this.state);
//      newState.pelicula = event.target.value;
//      newState[tipo] = event.target.value; 
//      this.setState(newState)
        this.setState({pelicula:event.target.value })
        console.log(event.target.value);
    }
    
    botonClick (){
        this.props.botonClick({pelicula: this.state.pelicula})
        console.log(this.state.pelicula);
    }    
     
    render(){
        return(
            <div className="Busqueda">
                 <input placeholder="Ingrese Pelicula" className="barra" value={this.state.pelicula} onChange={this.changeHandler.bind(this)}/><br/>
                 <button className="btonbusqueda" onClick={this.botonClick}>Buscar</button>
            </div>
        )
    }

}    