

export default class RestService {
   


  static llamada(url){
        return fetch(url)
            .then(function(response){
                let status = response.status;
                return (status >= 200 && status < 299) ?
                    response.json() : Promise.reject(response.status + ' : ' + response.statusText);
            })
    }
}